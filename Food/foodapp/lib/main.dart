import 'package:flutter/material.dart';
import 'package:foodapp/login.dart';
import 'package:foodapp/page1.dart';
import 'package:foodapp/page2.dart';
import 'package:foodapp/profile.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          scaffoldBackgroundColor: Colors.white),
      home: LoginPage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                child: Text(''),
                decoration: BoxDecoration(
                    color: Colors.blue[100],
                    image: DecorationImage(
                        image: AssetImage('images/pic.jpg'),
                        fit: BoxFit.cover)),
              ),
              Container(
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.black)),
                child: ListTile(
                  leading: Icon(Icons.person),
                  title: const Text('Profile'),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProfileScreen()));
                  },
                ),
              ),
              Container(
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.black)),
                child: ListTile(
                  leading: Icon(Icons.logout),
                  title: const Text('Logout'),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  },
                ),
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: const Text('Settings'),
              ),
            ],
          ),
        ),
        body: ListView(
          children: [
            Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MyHomePage1()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://media-cdn.tripadvisor.com/media/photo-s/07/f8/cd/74/caption.jpg'))),
                ),
              ),
            ),
            Center(
                child: Text(
              "ของคาว",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            )),
            Divider(height: 20),
            Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MyHomePage2()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://obs.line-scdn.net/0hm3HfSKeQMhx1Hx3pHMpNS09JMXNGcyEfESljHylxbChQJ3ZNTHB9clYYPCRaKXVCHHl6c1YeKS1ffSFDGn9-/w644'))),
                ),
              ),
            ),
            Center(
                child: Text(
              "ของหวาน",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            )),
          ],
        ));
  }
}
