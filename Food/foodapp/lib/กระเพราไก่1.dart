import 'package:flutter/material.dart';

class menu1 extends StatefulWidget {
  String name = "";
  String price = "";
  String owner = "";
  String img = "";
  String ig = "";
  String countdown = "";
  String method = "";

  menu1(
      {Key? key,
      required this.name,
      required this.price,
      required this.owner,
      required this.img,
      required this.ig,
      required this.countdown,
      required this.method})
      : super(key: key);

  @override
  _menu1State createState() => _menu1State(this.name, this.price, this.owner,
      this.img, this.ig, this.countdown, this.method);
}

class _menu1State extends State<menu1> {
  String name;
  String price;
  String owner;
  String img;
  String ig;
  String countdown;
  String method;

  _menu1State(this.name, this.price, this.owner, this.img, this.ig,
      this.countdown, this.method);

  @override
  void initState() {
    super.initState();
    setState(() {
      this.name = name;
      this.price = price;
      this.owner = owner;
      this.img = img;
      this.ig = ig;
      this.countdown = countdown;
      this.method = method;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(name + " สูตร" + owner),
      ),
      body: ListView(
        children: [
          Image.asset(
            img,
            width: 250,
            height: 250,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('วัตถุดิบ',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
              Text(
                countdown,
                style: TextStyle(color: Colors.red, fontSize: 25),
              ),
            ],
          ),
          Text(
            "\n" + ig,
            style: TextStyle(fontSize: 18),
          ),
          Text('\nวิธีทำ',
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
          Text(
            '\n' + method,
            style: TextStyle(fontSize: 18),
          ),
        ],
      ),
    );
  }
}
